<?php 
	function getProductoras($series)
	{
		$productoras = array();
		$indiceSerie = 0;
		$repetido = false;
		while($indiceSerie != count($series))
		{			
			foreach ($series[$indiceSerie] as $indice => $datoIndice) 
			{
				if($indice == "productora")
				{
					$repetido = false;
					foreach ($productoras as $key => $productora) 
					{
						if($datoIndice == $productora)
						{
							$repetido = true;
						}
					}
					if(!$repetido)
					{
						$productoras [] = $datoIndice;
					}					
				}				
			}
			$indiceSerie++;
		}
		return $productoras;
	}
 ?>

<?php
$series = array(array("nombre"=>"Juego de Tronos", "imagen" => "juego_tronos.jpg", "productora" => "HBO"),
				array("nombre" => "Jack Ryan", "imagen" => "jack_ryan.jpg", "productora" => "Amazon"),
				array("nombre" => "La casa de papel", "imagen" => "casa_papel.jpg", "productora" => "Netflix"),
				array("nombre" => "Stranger Things", "imagen" => "stranger.jpg", "productora" => "Netflix"),
				array("nombre"=>"Big Little Lies", "imagen" => "big_little_lies.jpg", "productora" => "HBO"),
				array("nombre"=>"Los Soprano", "imagen" => "soprano.jpg", "productora" => "HBO"),
				array("nombre" => "Downton Abbey", "imagen" => "downton.jpg", "productora" => "Amazon"),
				array("nombre" => "Black Mirror", "imagen" => "black_mirror.jpg", "productora" => "Netflix"),
				array("nombre"=>"True Detective", "imagen" => "true_detective.jpg", "productora" => "HBO"),
				array("nombre" => "The Crown", "imagen" => "crown.jpg", "productora" => "Netflix"),


);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Cándido Cobo</title>
</head>
<body>
	<h1>Series por productos</h1>
	<form method="post" action="">
		<?php 		
			$productoras = getProductoras($series);
			if(isset($_POST["productoras"]))
			{
				$productoraElegida = $_POST["productoras"];
				echo "<select name ='productoras'>"; 		 	
				foreach ($productoras as $indice => $productora) 
				{
					if($productoraElegida == $productora)
					{
						echo "<option value='$productora' selected>$productora</option>";
					}	
					else
					{
						echo "<option value='$productora'>$productora</option>";
					}
				}
				echo "</select><br>";
			}
			else
			{
				echo "<select name ='productoras'>"; 		 	
				foreach ($productoras as $indice => $productora) 
				{
					echo "<option value='$value'>$productora</option>";
				}
				echo "</select><br>";
				}
		 ?>
		 <input type="submit" name="Buscar">
	 </form>
	 <?php 
	 	if(isset($_POST["Buscar"]))
	 	{
	 		if(!empty($_POST["productoras"]))
	 		{
	 			$productora = $_POST["productoras"];
	 			$mensaje = "";
	 			$resultados = "";
	 			$nombre = "";
	 			$imagen = "";
	 			$indiceSerie = 0;		
	 			$totalSeries = 0;
	 			$resultados.="<table>";	
	 			$resultados.= "<tr>";	
	 			for ($i = 0 ; $i != count($series) ; $i++) 
	 			{
	 				$productoraSerie =  $series[$i]["productora"];
	 				if($productoraSerie == $productora)
					{
						$imagenNombre = $series[$i]["imagen"];
						$nombreSerie = $series[$i]["nombre"];
	 					$imagen.= "<td><img src = series/$imagenNombre></td>";
	 					$nombre = "<td>$nombreSerie</td>";
	 					$totalSeries++;										
						$resultados.=$imagen;
						$resultados.=$nombre;
						$imagen = "";
						$nombre = "";						
					}
					$resultados.= "</tr>";	 					
	 			}
	 			$resultados.= "</table>";		
				echo "$totalSeries encontradas para la productora $productora<br>";
				echo "$resultados";
	 			/*
				while($indiceSerie != count($series))
				{			
					$resultados.= "<tr>";
					foreach ($series[$indiceSerie] as $indice => $datoIndice) 
					{						
						if($datoIndice == $productora)
						{							
							foreach ($series[$indiceSerie] as $i => $dato) 
							{						
								if($i == "imagen")
								{														
									$imagen.= "<td><img src = series/$dato></td>";
									$totalSeries++;										
									$resultados.=$imagen;
									$resultados.=$nombre;
									$imagen = "";
									$nombre = "";
								}	
								if($i == "nombre")
								{									
									$nombre.= "<td>$dato</td>";									
								}
							}	
								
						}	
								
					}
					$indiceSerie++;					
					$resultados.= "</tr>";
				}
				$resultados.= "</table>";		
				echo "$totalSeries encontradas para la productora $productora<br>";
				echo "$resultados";
	 		}
	 		*/
	 	}
	 }
	  ?>

</body>
</html>