@extends('layouts.master')
@section('titulo')
Mostrando user
@endsection
@section('contenido')
  <br><br><br>
  <div class="row">
  	<div class="col-sm-3">
  		<img src="{{ url('/assets/imagenes') }}/{{ $user->rutaImagen }}" width="300">
  	</div>
  	<div class="col-sm-9">
      <h1>Perfil de <strong>{{ $user->name }}</strong></h1>
      <h2>Mostrando retos creados por {{ $user->name }}</h2>
      @foreach($user->retos as $reto)
        <a href="{{ url('retos')}}/{{$reto->categoria->categoria }}/{{$reto->id}}">{{ $reto->nombre }}</a><br>
      @endforeach
  	</div>
  </div>
  <a href="{{ url('/retos') }}" class="btn btn-primary">Volver al listado</a>
  @if(Auth::user()->name == $user->name)
    <a href="{{ url('/editar') }}" class="btn btn-secondary">Editar perfil</a>
  @endif
@endsection
