<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">    
    <script type="text/javascript" src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <title>@yield("titulo")</title>
  </head>
  <body>
    @include("partials.navbar")
     <div class="container-fluid">
    @yield("contenido")
    </div>
  </body>
</html>
