@extends('layouts.master')
@section('titulo')
Index
@endsection
@section('contenido')
  <br>
  <br>
  <br>
  <br>
  <div class="album py-5 bg-light">
  <div class="container">
  <div class="row">  
 
  @foreach( $categorias as $c )
    <div class="col-md4">
      <div class="card mb-4 shadow-sm">
      <div class="col-xs-12col-sm-6col-md-4">
        <a href="{{ url('retos/'.$c->categoria) }}">
          <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
          xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false"
          role="img" aria-label="Placeholder">
          <!--<text x='50%' y='50%' fill='#eceeef' dy='.3em'><img src='https://image.flaticon.com/icons/svg/457/457088.svg' width='100%' height='100%' />-->
            <title>{{ $c->categoria }}</title>
            <rect width="100%" height="100%" fill="#55595c"/>
            <text x="50%" y="50%" fill="#eceeef" dy=".3em">
              {{ $c->categoria }}
            </text>
        </svg>
        Total retos: {{ $c->getCountRetos($c->categoria) }}
        </a>
        <div class="card mb-4 shadow-sm">
        <div class="card-body">
          <p class="card-text">{{ $c->descripcion }}</p>
        </div>
      </div>
    </div>
    </div>
  </div>
  @endforeach
</div>
</div>
</div>
@endsection
