@extends('layouts.master')
@section('titulo')
Retos
@endsection
@section('contenido')
  <br>
  <br><br>
  <br>
  <div class='table-responsive'>
  <table class='table table-striped table-sm'>
  <thead>
  <td>Nombre</td><td>Descripcion</td><td>Autor</td></thead>
  <tbody>
 
  @foreach( $retos as $r )
    <tr>
      <td>
        <a href="{{ url('retos/'.$r->getCategoria($r->idCategoria).'/'.$r->nombre) }}">{{ $r->nombre }}</a>
      </td>
      <td>{{ $r->descripcion }}</td>
      <td>
        {{$r->user}}
        
      </td>
    </tr>
  @endforeach
  </tbody>
  </table>
  </div>
@endsection
