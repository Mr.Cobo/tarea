@extends('layouts.master')
@section('titulo')
Mostrando reto
@endsection
@section('contenido')

@inject('categoria','App\Categoria')
  <br><br><br><br><br>
  <div class="row">
  	<div class="col-sm-3">
  	</div>
  	<div class="col-sm-9">
  		<h1>{{ $reto->nombre }}</h1>
      <strong class="d-inline-block mb-2 text-primary">{{ $reto->puntos }} Puntos</strong><br>
      <strong>Autor:
      <a href="{{ url('users/'.$reto->user->name) }}">{{ $reto->user->name }}</a>
      </strong>
  		<p>{{ $reto->descripcion }}</p>
      {{$reto->getRutaCategoria($reto->idCategoria).$reto->rutaReto}}
      <a class="btn btn-secondary my-2"
      href="{{ $reto->getRutaCategoria($reto->idCategoria).$reto->rutaReto }}">
      Empezar reto
      </a>
      <br>
  	</div>
  </div><br>
  <a href="{{ url('/retos') }}" class="btn btn-primary">Volver al listado</a>
@endsection
