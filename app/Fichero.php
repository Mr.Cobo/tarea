<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fichero extends Model
{
    protected $table = 'ficheros';

    public function retos()
    {
      return $this->belongsToMany(Reto::class,
        'retos','idFichero','idReto');
    }
}
