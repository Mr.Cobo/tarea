<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function getProfile($nombre)
  {
    $idUser =
    User::where('name',$nombre)
    ->pluck('id')
    ->all()[0];
    return view('user.mostrar', array('user' => User::findOrFail($idUser)));
  }

  public function getEditProfile()
  {
  	if (isset(Auth::user()->name))
  	{
  		$user = Auth::user()->name;
  		$idUser =
	    User::where('name',$user)
	    ->pluck('id')
	    ->all()[0];
	    return view('user.editar', array('user' => User::findOrFail($idUser)));
  	}
  	return redirect("/");
  }

  public function postEditar(Request $request)
  {
  		$user = Auth::user()->name;
  		$idUser =
	    User::where('name',$user)
	    ->pluck('id')
	    ->all()[0];
  		$perfil = User::where('id',$idUser)->first();    	    	
    	$perfil->name = $request->nombre;   	
    	if(!empty($request->imagen && $request->imagen->isValid()))	
		{
			Storage::disk('imagenes')->delete($perfil->imagen);
			$perfil->rutaImagen = $request->imagen->store('','imagenes');
		}

    	try
    	{
    		if($perfil->save())
	    	{    	
	    		return redirect('/')->with('mensaje','Perfil editado...');;
	    	}
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return redirect('/')->with('mensaje','FallO');
    	}
  }
}
