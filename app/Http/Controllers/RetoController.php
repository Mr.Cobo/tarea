<?php

namespace App\Http\Controllers;

use App\Fichero;
use Illuminate\Support\Facades\Auth;
use App\FicherosProyecto;
use App\Reto;
use App\Categoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class RetoController extends Controller
{
    public function getCrear()
    {
      return view('retos.addReto');
    }

    public function postCrear(Request $request)
    {
        $name = $request->nombre;
        $descripcion = $request->descripcion;
        $categoria = $request->categoria;
        $idCategoria =
        Categoria::where('categoria',$categoria)
        ->pluck('id')
        ->all()[0];
        $rutaCategoria =
        Categoria::where('categoria',$categoria)
        ->pluck('rutaCategoriaServidor')
        ->all()[0];
        $idRetoMaxCategoria = Reto::where('idCategoria',$idCategoria)->count();
        $fichero = $request->proyecto;
        $autor = Auth::user()->id;
        $puntos = $request->puntos;
        //RETO OBJECT
        $reto = new Reto();
        Categoria::where('id',$idCategoria)
        ->pluck('rutaCategoriaServidor')
        ->all()[0];
        $reto->nombre=$name;
        $reto->descripcion=$descripcion;
        $reto->puntos=$puntos;
        $reto->idAutor=$autor;
        $reto->idCategoria=$idCategoria;        
        $reto->rutaReto = "reto".$idRetoMaxCategoria;
        Storage::disk('custom')->makeDirectory($categoria."/".$reto->rutaReto);
        $path = $fichero->storeAs(
          $categoria."/".$reto->rutaReto,'index.php', 'custom'
        );
        //FILE OBJECT
        $f = new Fichero();
        $f ->ruta=$path;
        $f->save();
        if($reto->save())
        {
          $reto->ficheros()->attach($f);     
          return view('retos.index',array('categorias' => Categoria::all()));
        }
    }

    public function editReto($id)
    {
        return view('retos.editReto', array('reto' => $this -> retos[$id]));
    }

    public function deleteReto($id)
    {
        return view('retos.deleteReto', array('reto' => $this -> retos[$id]));
    }

    public function getReto($categoria,$reto)
    {
      $idReto =
      Reto::where('nombre',$reto)
      ->pluck('id')
      ->all()[0];

      return view('retos.mostrar', array('reto' => Reto::findOrFail($idReto)));
    }

    public function getRetosCategoria($categoria)
    {
      $id =
      Categoria::where('categoria',$categoria)
      ->pluck('id')
      ->all()[0];
      return view('retos.retosCategoria', array('retos' =>
                  Reto::where('idCategoria','=',$id)->get()));
    }

    public function getRetos()
    {
        return view('retos.index',array('categorias' => Categoria::all()));
    }
}
