<?php

namespace App;

use App\Reto;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    public function getCountRetos($categoria)
    {
      $id = Categoria::where('categoria',$categoria)
      ->pluck('id')
      ->all()[0];
      return Reto::where('idCategoria',$id)->count();
    }
}
