<?php

namespace App;

use App\Categoria;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Reto extends Model
{
    //
    public function getCategoria($idRetoCategoria)
    {
       $categorias = Categoria::all();
       foreach($categorias as $categoria)
       {
         if($categoria->id == $idRetoCategoria)
         {
            return $categoria->categoria;
         }
       }
       return null;
    }

    public function getRutaCategoria($idCategoria)
    {
       $categorias = Categoria::all();
       foreach($categorias as $categoria)
       {
         if($categoria->id == $idCategoria)
         {
            return $categoria->rutaCategoriaServidor;
         }
       }
       return null;
    }

    public function ficheros()
    {
      return $this->belongsToMany(Fichero::class,'ficherosProyecto',
      'idReto','idFichero');
    }

    public function categoria()
    {
      return $this->hasOne(Categoria::class,
      'id','idCategoria');
    }

    public function user()
    {
      return $this->hasOne(User::class,'id','id');
    }
}
