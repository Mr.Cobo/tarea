<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retos', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('rutaReto');
            //$table->bigInteger('idRetoFicheros')->unsigned();
            $table->bigInteger('idCategoria')->unsigned();
            $table->foreign('idCategoria')->references('id')->on('categorias');
            $table->string('descripcion');
            $table->integer('puntos');
            $table->bigInteger('idAutor')->unsigned();
            $table->foreign('idAutor')->references('id')->on('users');
           // $table->decimal('valoracion',2,1);
            //$table->string('flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retos');
    }
}
