<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFicherosProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficherosProyecto', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('idFichero')->unsigned();
            $table->bigInteger('idReto')->unsigned();
            $table->foreign('idFichero')->references('id')->on('ficheros');
            $table->foreign('idReto')->references('id')->on('retos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficherosProyecto');
    }
}
