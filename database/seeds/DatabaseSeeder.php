<?php
use App\Categoria;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  private $categorias = [
                         [
                         "nombre"=>"Accion",
                         "descripcion"=>"",
                         "ruta"=>"http://localhost/juegos/Accion/",
                         "logo"=>""
                         ],
                         [
                         "nombre"=>"Music Games",
                         "descripcion"=>"",
                         "ruta"=>"http://localhost/juegos/Music-Games/",
                         "logo"=>""
                         ]
                        ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categorias')->delete();
      foreach ($this->categorias as $categoria)
      {
        $c = new Categoria();
        $c->categoria = $categoria["nombre"];
        $c->descripcion = $categoria["descripcion"];
        $c->rutaCategoriaServidor = $categoria["ruta"];
        $c->logo = $categoria["logo"];
        $c->save();
      }

    }
}
