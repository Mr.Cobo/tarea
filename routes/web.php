<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//INICIO
Route::get('/', 'IntroController@getIntro');

//RETOS
Route::get('retos/crear','RetoController@getCrear');
Route::post('retos/crear','RetoController@postCrear');

Route::get('retos','RetoController@getRetos');

Route::get('retos/{categoria}','RetoController@getRetosCategoria');

Route::get('retos/{categoria}/{reto}','RetoController@getReto');

//LOGIN
Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
  	Route::get('/editar', 'UserController@getEditProfile');
	Route::post('/editar', 'UserController@postEditar');
});

//USUARIOS
Route::get('/users/{user}', 'UserController@getProfile');
Route::get('/users', 'IntroController@getIntro');
